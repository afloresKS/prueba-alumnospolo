class CreateStudents < ActiveRecord::Migration[5.0]
  def change
    create_table :students do |t|
      t.string :name
      t.string :last_name
      t.integer :age
      t.integer :average

      t.timestamps
    end
  end
end
